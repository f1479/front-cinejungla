FROM node:17

WORKDIR /app

COPY package.json .

RUN npm install

COPY . .

ENTRYPOINT ["npm"]

CMD ["run","serve"]