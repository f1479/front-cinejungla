const api = 'http://localhost/api';

const routes = {
    movie: '/movies',
    function: '/functions',
    snack: '/food',
    carouselFood: '/carousel-food',
    signUp: '/auth/signup',
    login: '/auth/login',
    logout: '/auth/logout',
    user: '/auth/user',
    numberChair: '/number-chairs',
    chairsAvailable: '/chairs-available',
    payPurchase: '/pay-purchase'
}

export default class Api {
    movies(page){
        return get(`${routes.movie}?page=${page}`)
    }
    snacks(page){
        return get(`${routes.snack}?page=${page}`)
    }
    internalMovie(path){
        return get(`${routes.movie}/${path}`)
    }
    availableFunctions(pathMovie, date){
        return get(`${routes.function}/${pathMovie}/${date}`)
    }
    carouselFood(){
        return get(routes.carouselFood); 
    }

    //AUTH
    async singUp(body){
        return await post(routes.signUp, body);
    }
    async login(body){
        const request = await post(routes.login, body);
        if(request.access_token){
            localStorage.setItem('uts', request.access_token);
            location = '/'
        }
        return request;
    }
    async logout(){
        const request = await get(routes.logout);
        localStorage.removeItem('uts');
        location.reload();
        return request;
    }
    user(){
        return get(routes.user)
    }

    numberOfChairsAndPrice(idFunction){
        return get(`${routes.numberChair}/${idFunction}`);
    }
    chairsAvailable(typeChair, idFunction){
        return get(`${routes.chairsAvailable}/${typeChair}/${idFunction}`);
    }
    payPurchase(info){
        return postText(routes.payPurchase, info)
    }
}


const get = (url) => {
    return new Promise(( resolve ) => {
        fetch(`${api}${url}`, {
            method: 'GET',
            headers: headers(),
        })
        .then(res => res.json())
        .then(data => {
            resolve(data);
        })
    })
}

function post(url, body){
    return new Promise((resolve) => {
        fetch(`${api}${url}`, {
            method: 'POST',
            headers: headers(),
            body: JSON.stringify(body ? body : {}),
        })
        .then( res => {
            if(!res.ok){
            throw res.status
            }
            return res.json()
        })
        .then( data => {
            resolve(data)
        })

    })
}
function postText(url, body){
    return new Promise((resolve) => {
        fetch(`${api}${url}`, {
            method: 'POST',
            headers: headers(),
            body: JSON.stringify(body ? body : {}),
        })
        .then( res => {
            if(!res.ok){
            throw res.status
            }
            return res.text()
        })
        .then( data => {
            resolve(data)
        })

    })
}

const headers = () => {
    const uts = localStorage.getItem('uts');
    let headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("X-Requested-With", "XMLHttpRequest");
    uts && headers.append("Authorization", `Bearer ${uts}`);
    return headers;
}

