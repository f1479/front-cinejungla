import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: () => import(/* webpackChunkName: "home" */ '../views/general/Movie.vue')
  },
  {
    path: '/snacks',
    name: 'snacks',
    component: () => import(/* webpackChunkName: "snacks" */ '../views/general/Snack.vue')
  },
  {
    path: '/auth',
    name: 'auth',
    component: () => import(/* webpackChunkName: "auth" */ '../views/LoginView.vue')
  },
  {
    path: '/detalle/:movie',
    name: 'detail',
    component: () => import(/* webpackChunkName: "detail" */ '../views/general/InsidePage.vue'),
  },
  {
    path: '/404',
    name: 'notFound',
    component: () => import(/* webpackChunkName: "detail" */ '../views/general/NotFound.vue'),
  },
  {
    path: '*',
    component: () => import(/* webpackChunkName: "detail" */ '../views/general/NotFound.vue'),
  },


]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
